#/usr/bin/env python2

import fontforge
import glob
import sys
import lxml.etree as ET
import psMat

FontName = sys.argv[1]
#yTranslate = sys.argv[2]
SVG_DIR = glob.glob('svg/'+FontName+'/*.svg')
font = fontforge.open('temp/empty.sfd')
compositeChar = [192, 193, 194, 195, 196, 199, 200, 201, 202, 203, 204, 205, 206, 207, 210, 211, 212, 213, 214, 217, 218, 219, 220, 224, 225, 226, 227, 231, 232, 233, 234, 235, 236, 237, 238, 239, 242, 243, 244, 249, 250, 251, 252, 350, 351]

for glyph in SVG_DIR:
    with open(glyph, 'rt') as f:
        treeLet = ET.parse(f)
    rootLet = treeLet.getroot()
    chasse = rootLet.get('width')

    letter = glyph.split("/")[-1].replace(".svg", "")
    ##print('dec : ' + letter + '| width : ' + chasse)

    letter_char = font.createMappedChar(letter)
    letter_char.importOutlines(glyph)
    letter_char.removeOverlap()
    letter_char.simplify()
    letter_char.width = int(chasse)
    letter_char.left_side_bearing = 2
    letter_char.right_side_bearing = 2

#for letter_comp in compositeChar:
#    glyphAcc = font.createChar(letter_comp)
#    glyphAcc.build()
#    print('-----')
#    print(letter_comp)
#    print('-----')
#    glyphAcc.left_side_bearing = glyphAcc.right_side_bearing = (glyphAcc.left_side_bearing + glyphAcc.right_side_bearing)/2



espace = font.createChar(32)
espace.width = font['a'].width
espacefine = font.createChar(8201)
espacefine.width = int(font['a'].width / 3)
font.em = 1000

#trs = psMat.translate(0, int(yTranslate)) 
#font.selection.all()
#font.transform(trs)

font.fontname = FontName
font.familyname = FontName
font.generate('FINAL/' + FontName + '.otf')

print (FontName)