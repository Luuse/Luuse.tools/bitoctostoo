import fontforge
from PIL import Image
import csv, glob, os
import numpy as np
import svgwrite

gly_Export = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","one","two","three","four","five","six","seven","eight","nine","zero","cedilla","grave","plus","less","equal","greater","comma","period","slash","colon","semicolon","question","at","backslash","ampersand","percent","quotesingle","asterisk","exclam","hyphen","underscore","parenleft","bracketleft","braceleft","parenright","bracketright","braceright","ae","AE","grave","acute","dieresis","cedilla",]

def export_bitmap(font_file, size):
    font = fontforge.open(font_file)
    for gly in font.glyphs():
        if gly.glyphname in gly_Export :
            gly.export("bitmap/" + gly.glyphname + ".bmp", size)
        print(gly.glyphname)
    print(font)

def bitmap_to_csv(bitmap_file):
    basename = os.path.basename(bitmap_file).replace('.bmp', '')
    im = Image.open(bitmap_file, 'r')
    listPix = list(im.getdata())
    i = 0
    with open( 'csv/'+basename+".csv", 'w') as f:
        writer = csv.writer(f)
        Lts = []
        for row in range(im.height):
            L = [] 
            for line in range(im.width):
                if listPix[i] == 0:
                    L.append('.')
                else:
                    L.append('$$$$$$$$')
                i += 1
            Lts.append(L)
            print(Lts)
        writer.writerows(Lts)


def csv_to_svg(csv_file, detectH, addH, detectW, addW, fontName):
    naddH = str(addH)
    naddW = str(addW)
    basename = os.path.basename(csv_file).replace('.csv', '')
    with open(csv_file, 'r') as file:
        reader = csv.reader(file)
        list_csv = []
        for row in reader: 
            list_csv.append(row)
        width = len(list_csv[0])
        height = len(list_csv)
        
        nbrW = 0
        nbrH = 0

        for compte in list_csv : 
            for combien in compte : 
                if combien == detectW :
                    nbrW = nbrW + 1
                if combien == detectH :
                    nbrH = nbrH + 1
 
        if nbrH != 0 :
            addH = addH / nbrH
        if nbrW != 0 :
            addW = addW / nbrW


        ### élargissement width

        for f in list_csv : 
            for e in f :

                if e == detectW : 
                    placeY = f.index(e)
                    f[placeY] = '$$$$$$$$'
                    for s in range(int(addW)) :
                        for g in list_csv : 
                            g.insert(placeY, g[placeY])


                width = len(list_csv[0])

        ### élargissemment height

        for r in list_csv : 
            for s in r :
                inserthere = list_csv.index(r) 

                if s == detectH :
                    placeX = r.index(s)
                    r[placeX] = '$$$$$$$$'
                    for i in range(int(addH)) : 
                        print (addH)
                        list_csv.insert(inserthere,r)



        height = len(list_csv)


        dwg = svgwrite.Drawing('test.svg', size=(width, height))
        dwg.viewbox(0, 0, width, height)
        y = 0
        for line in list_csv:
            x = 0
            for val in line: 
                #print(val)
                #print('---------')
                x += 1
                if val == '$$$$$$$$':
                    part = dwg.rect(insert=(x, y), size=(1, 1), rx=None, ry=None, fill='black')
                    dwg.add(part)

            y += 1
            # if val[0] == 255:
            #     part = dwg.ellipse(center=(val[1] + 1, val[2] + 0.5), r=(.5, .5), fill='black')

        
        if not os.path.exists('svg/'+ fontName + naddH + "_" + naddW + '/'):
            os.makedirs('svg/'+ fontName + naddH + "_" + naddW + '/')

        dwg.saveas('svg/'+ fontName + naddH + "_" + naddW + '/'+basename+'.svg')

def export_svg(files , motH, nombreH , motW , nombreW, fontName):
    for csv in glob.glob(files):
        csv_to_svg(csv, motH, nombreH , motW , nombreW, fontName)
            

def export_csv(files):
    for bitmap in glob.glob(files):
        bitmap_to_csv(bitmap)





#export_bitmap('input/Sony-24x12-nonMono.ttf', 24)
#export_csv('bitmap/*.bmp')
#export_svg('csv/*.csv', "v" , 2 , "h" , 2, "sonymono")
#csv_to_svg("csv/*.csv", "v" , 0 , "h" , 12,"standard")

mXrange = 64
z = 0
while z < mXrange : 
    for i in range(0, mXrange):
        if (i % 4)== 0 : 
            export_svg('csv/*.csv', "v" , i , "h" , z, "sonymono")

    export_svg('csv/*.csv', "v" , z , "h" , z, "sonymono")
    z += 4



# print(dir(csv))
