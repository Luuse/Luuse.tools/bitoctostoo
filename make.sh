#!/bin/bash

for yol in {0..64}; do
    if (( $yol % 4 )); then
        echo $yol
    else 
        for yel in {0..64}; do
            if (( $yel % 4 )); then
                echo $yel
            else
                python3 import.py sonymono"$yel"_"$yol"
            fi 
        done
    fi
done